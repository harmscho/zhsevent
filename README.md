# ZHSEvent

Tools for handling ZHAireS simulations

The dependencies for this software package are:
*  ROOT (https://root.cern.ch/)
*  FFTW (http://www.fftw.org)