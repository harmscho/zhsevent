#ifndef _ZHSVIEWER_h_
#define _ZHSVIEWER_h_


#include <TObject.h>
#include <TGFrame.h>
#include <TGClient.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TRandom.h>
#include <TGButton.h>
#include <TGComboBox.h>
#include <TGNumberEntry.h>
#include <TGFrame.h>
#include <TRootEmbeddedCanvas.h>
#include <TGraph.h>
#include <TGTab.h>
#include <string>
#include <RQ_OBJECT.h>
#include <TQObject.h>
#include <TLine.h>
using namespace std;

class TGWindow;
class TGMainFrame;
class TRootEmbeddedCanvas;
class Antenna;
class ZHSEvent;
//class TGraph;

class ZHSViewer : public TGMainFrame {
  RQ_OBJECT("ZHSViewer");
private:
  TRootEmbeddedCanvas *fEcanvas;
  TRootEmbeddedCanvas *fMapCanvas;
  TGMainFrame *fMain;
  TGComboBox *drawModeSel;
  TGNumberEntry *fLowerFreq;
  TGNumberEntry *fUpperFreq;
  ZHSEvent* fCurrentEvent;
  TGTab *fTab ;
  TGCompositeFrame *fWaveTab;
  TGCompositeFrame *fMapTab;
  string fSryFile;
  string fTimeFile;
  int fNPulse;
public:
  ZHSViewer(const TGWindow *p,UInt_t w,UInt_t h,string sryFile, string timeFile,int nPulse = 0);
  ~ZHSViewer();
//  virtual ZHSViewer();
  void ReadEvent();
  void DoDraw();
  void DrawMap();
  void Update();
  void GetMinMaxInGraph(TGraph* gr,double& min, double& max);
  double GetExtremumInGraph(TGraph* gr);
  TGraph* AntennaToGraph(Antenna a,std::string component);
  TGraph* AntennaToGraph(Antenna a,std::string component,std::string name);

  ClassDef(ZHSViewer,0)

};
#endif
