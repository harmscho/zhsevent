/*
 Simple Library to read in the time dependent electric fields for an array of antenas generated with ZHAireS simulations. Some standard (fft) routines on the time dependent electric field are added. This library dependends on fftw.
 
 
 The coordinate system used in this library AIRES coordinate system:
 The origion of the coordinate system is at the location where the shower axis crosses the ground plane.
 the x axis is positve in the geomagnetic north direction
 The y axis is paralel to geomagnetic west
 The zenith angle is measured from the vertical at the shower core
 The azimuth angles are defined from the north positive towards east
 
 NOTE GEOMAGNETIC DECLINATION AND INCLINATION ARE CONVERTED TO THE AZIMUTH / ZENITH
 Coordinate system.
 

 TODO: Fix weird Z coordinate ZHS properly (injection point - ground)
 
 Harm Schoorlemmer July 23 2013
 */

#ifndef _ZHSEVENT_
#define _ZHSEVENT_


#include <iostream>
#include <string>
#include <vector>
#include "TObject.h"

using namespace std;
//namespace  ZHSUtil{};
//class ZHSViewer;

class Antenna: public TObject {
public:
  Antenna();
  virtual ~Antenna();
  vector<float> fEx;// V/m
  vector<float> fEy;// V/m
  vector<float> fEz;// V/m
  double fX,fY,fZ;//location
  double fTimeStep;//sampling frequency
  double fFreqBin;//frequency bin width
  double fStartTime;
  int fId;
  bool fIsTimeDomain;
  
  
  double fPeakTime;
  double fWindow;
  double fWindowStart;
  double fI;
  double fQ;
  double fU;
  double fV;
  double fIp;
  double fL;
  double fA;
  double fB;
  double fPhi;
  
  ClassDef(Antenna,0);
};



class ZHSEvent: public TObject {
public:
  //constructors
  ZHSEvent();
  virtual ~ZHSEvent();
  ZHSEvent(string filename);
  ZHSEvent(string efieldFilename,string parMetersFilename);

  //fill the data from the timefersnel-root.dat or *.zhaires
  void FillFieldFromFile(string filename);

  //fill the data from the *.sry file
  void FillShowerProperties(string filename);
  
  //shower properties
  void SetEnergy(float _energy) { fEnergy = _energy;};
  void SetZenithAngle(float _zen) { fZenith = _zen;};
  void SetAzimuthAngle(float _azi) { fAzimuth = _azi;};
  void SetXmax(float _xmax) {fXmax = _xmax; };
  void SetXfirst(float _xfirst) {fXfirst = _xfirst;};
  void SetXmaxVert(float _xmaxVert) {fXmaxVert = _xmaxVert; };
  void SetXfirstVert(float _xfirstVert) {fXfirstVert = _xfirstVert;};
  void  SetGroundAlt(float _groundAlt) {fGroundAlt = _groundAlt;};

  float GetEnergy() { return fEnergy;};
  float GetZenithAngle() {return fZenith;};
  float GetAzimuthAngle() { return fAzimuth;};
  float GetXmax() {return fXmax;};
  float GetXfirst() {return fXfirst;};
  float GetXmaxVert() {return fXmaxVert;};
  float GetXfirstVert() {return fXfirstVert;};
  float GetGroundAlt(){return fGroundAlt;};
  
  //Geomagnetic field properties
  void SetGeomagneticIntesity(float _intens) {fBint = _intens;};
  void SetGeomagneticAzimuth(float _azi) {fBazi = _azi;};
  void SetGeomagneticZenith(float _zen) {fBzen = _zen;};
  float GetGeomagneticIntesity() {return fBint;};
  float GetGeomagneticAzimuth() {return fBazi;};
  float GetGeomagneticZenith() {return fBzen;};

  
  //applying some techniques to the time series

  //Rectangular bandpass filter between lowFreq and highFreq. Frequencies in Hertz
  void Filter(double lowFreq, double highFreq);
  
  //calculate the envelope of the analytical signal using a hilbetr transform
  void Envelope();

  //Frequencies in Hertz
  void FilterAndEnvelope(double lowFreq, double highFreq);
  
  //Add gaussian noise to simulations.
  void AddGaussNoise(double rms, bool randomSeed = false);
  
  void ScaleEfield(double k);
  //calculates the amplitude spectrum of the electric field data
  //NOTE that the units that the fArrayUser now holds information
  // in the frequency domain. 
  void AmplitudeSpectrum();//Calculates the amplitude spectrum

  //print the ID, and location of the antenna's
  void PrintArray();

  //print the electric field data for a given antenna
  void PrintEfield(int antId,string comp);//comp = x,y, or z
  
  void PrintEfieldUser(int antId,string comp);//comp = x,y, or z
 
  Antenna GetAntenna(int antId);
  
  Antenna GetAntennaUser(int antId);
  
  void GetLastApplied(){
    cout << fLastApplied << endl;
  };
  
  int GetNAntennas() {
    return fArray.size();
  }
  
  void PrintEventInfo();
  
  
  void RecoAntenna(Antenna& a);
  void PrintAntennaReco(Antenna& a);

  //The Array of antennas holding the time dependent electric field at a given location
  vector <Antenna> fArray;
  
  //If the user calls one of the "fft" functions, it is applied ont the original fArray data
  //and stored in the fArrayUser.
  vector <Antenna> fArrayUser;
  
//  private:
  string fLastApplied;
  
  
  float fZenith;//rad
  float fAzimuth;//rad
  float fEnergy;//eV
  float fXfirst;//g / cm^2
  float fXmax;//g / cm^2
  float fXfirstVert;//g / cm^2
  float fXmaxVert;//g / cm^2
  float fBint;//Tesla
  float fBzen;//rad
  float fBazi;//rad
  float fGroundAlt;
  
  ClassDef(ZHSEvent,0)
};
#endif
