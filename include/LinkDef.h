#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class Antenna+;
#pragma link C++ class ZHSEvent+;
#pragma link C++ class ZHSViewer+;

//#pragma link C++ namespace ZHSUtil;

#endif