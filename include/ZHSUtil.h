/* 
 Utitlities for doing calculations on time series and fourrier transformation. etc etc
 Harm Schoorlemmer July 24, 2013
*/





#ifndef _ZHSUtil_h_
#define _ZHSUtil_h_

#include <time.h>
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include "fftw3.h"
#include <algorithm>
#include <math.h>


#define J2000 2451545.0
#define Dtohr 0.066666666666666
#define Hrtod 15.

using namespace std;

namespace ZHSUtil{

  //say hi
  void SayHi();
  //Fourier transform, returns a complex fft.
  void FFT(vector<float>& norm, fftw_complex* &spec);
  //Inverse Fourier transform, returns a real vector.
  void InvFFT(fftw_complex* &fftin,vector<float>& v_out, int len);
  //Absolute value of the complex fft
  void AbsFFT(vector<float>& v_in,vector<float>& v_out);
  //Phase value of the complex fft
  void PhaseFFT(vector<float>& v_in,vector<float>& v_out);  
  //HilbertEnvelope;
  void HilbertEnvelope(vector<float>& v_in,vector<float>& v_out);
  //Preforming a Median Filter to a vector, WindowSize default = 3
  void MedianFilter(vector<float>& v_in, vector<float>& v_out,int WindowSize);
  ///Returns the Absolute frequency spectrum of a vector after applying a medium filter to it, WindowSize default = 3 
  void AbsFFTMedianFilter(vector<float>& v_in, vector<float>& v_out,int WindowSize);
  void AbsFFTMedianFilter(vector<float>& v_in, vector<float>& v_out);
  // RFI supression using the medium filter
  // FFT is applied to vector on time domain 
  // Then on the absolute value of the frequency domain a medium filter is applied
  // The phase is conserved and than the vector is transformed back into the time domain
  void RFISupressionMedianFilter(vector<float>& v_in, vector<float>& v_out,int WindowSize);
  void FFTInvFFT (vector<float>& v_in, vector<float>& v_out);
  // Simple rectagular filter
  void RectangularFilter(vector<float>& v_in,vector<float>& v_out,double sample_frequency,double low_freq,double high_freq);

  //Calculates the group delay
  void GroupDelay(vector<float>& v_in,vector<float>& v_out,double df);
  
   //do time shift per frequency
  void TimeShiftFrequencyDomain(vector<float>& v_in,vector<float>& v_out,double sample_freq, double dt);
  
  //To make smooth windows...
  void HannFunction(vector<float>& v_in, vector<float>& v_out, double TraceFraction);
  //Dont use..
//  void HammingFunction(vector<float>& v_in, vector<float>& v_out, double);
  void HilbertTransform(vector<float>& v_in,vector<float>& v_out);
  void TimeSeriesI(vector<float>& v_in_x,vector<float>& v_in_y,vector<float>& v_out);
  void TimeSeriesQ(vector<float>& v_in_x,vector<float>& v_in_y,vector<float>& v_out);
  void TimeSeriesU(vector<float>& v_in_x,vector<float>& v_in_y,vector<float>& v_out);
  void TimeSeriesV(vector<float>& v_in_x,vector<float>& v_in_y,vector<float>& v_out);
  
  

};
#endif
