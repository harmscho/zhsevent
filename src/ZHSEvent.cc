
#include "ZHSEvent.h"
#include "ZHSUtil.h"

#include <sstream>
#include <fstream>

#include <iomanip>

#include "TRandom3.h"
using namespace ZHSUtil;

Antenna::Antenna(){}
Antenna::~Antenna(){}


//default contructor
ZHSEvent::ZHSEvent(){
}

ZHSEvent::~ZHSEvent(){
}

//contructor that opens file
ZHSEvent::ZHSEvent(string filename):fZenith(0),
fAzimuth(0),
fEnergy(0),
fXfirst(0),
fXmax(0),
fXfirstVert(0),
fXmaxVert(0),
fBint(0),
fBzen(0),
fBazi(0)
{
  FillFieldFromFile(filename);
}

ZHSEvent::ZHSEvent(string efieldFilename,string parMetersFilename)
{
  FillFieldFromFile(efieldFilename);
  FillShowerProperties(parMetersFilename);
}

void
ZHSEvent::FillFieldFromFile(string filename)
{
  if (fArray.size() != 0) {
    cout << "WARNING: over writing previous event " << endl;
    fArray.clear();
  }
  
  ifstream ifs(filename.c_str());
  int prevAntennaId = -1;
  string dum;
  double t,tprev,Ex,Ey,Ez,X,Y,Z;
  int Id;
  bool timeStepIsSet = false;
  Antenna a;
  if (ifs) {
    string line;
    while(ifs) {
      getline(ifs,line);
      //cout << line << endl;
      if (line[0] != '#') {
        stringstream ss(line);
        ss >> dum >> Id >> X >> Y >> Z >> t >> dum >> dum >> dum >> dum >> dum >> Ex >> Ey >> Ez;
        if (prevAntennaId != -1 && prevAntennaId != Id) {//clearing and adding to Array
          a.fIsTimeDomain = true;
          a.fFreqBin = 1./(a.fTimeStep * a.fEx.size());
          fArray.push_back(a);
          a.fEx.clear();
          a.fEy.clear();
          a.fEz.clear();

        }
        if (prevAntennaId != Id){//setting the begin time
          a.fStartTime = t * 1e-9;
        }
        a.fEx.push_back(Ex);
        a.fEy.push_back(Ey);
        a.fEz.push_back(Ez);
        a.fX = X;
        a.fY = Y;
        a.fZ = 100.e3 - Z;
        a.fId = Id;
        if (prevAntennaId == Id && !timeStepIsSet) {
          a.fTimeStep = (t - tprev) * 1e-9;//using seconds
          //cout << t << "\t" << tprev << "\t" << a.fTimeStep << endl;
          timeStepIsSet = true;
        }
        tprev = t;
        prevAntennaId = Id;
        
      }
      
    }
    a.fIsTimeDomain = true;
    a.fFreqBin = 1./(a.fTimeStep * a.fEx.size());
    fArray.push_back(a);
    
  } else {
    cout << "Error something wrong with filename: " << filename << endl;
  }
  
}

void
ZHSEvent::FillShowerProperties(string filename)
{
  ifstream ifs(filename.c_str());
  if (!ifs) {
    cout << "ERROR: Something wrong with "<< filename << endl;
    exit(0);
  }

  string line,dum,unit;
  bool foundEnergy = false;
  while (ifs) {
    getline(ifs,line);

    std::size_t found = line.find("Primary energy");
    if (found!=std::string::npos && !foundEnergy) {
      stringstream ss(line);
      double energy;
      ss >> dum >> dum >> energy >> unit;
      if (unit == "ZeV") {
        energy *= 1e21;
      } else if (unit == "EeV") {
        energy *= 1e18;
      } else if (unit == "PeV") {
        energy *= 1e15;
      } else if (unit == "TeV") {
        energy *= 1e12;
      } else if (unit == "GeV") {
        energy *= 1e9;
      } else if (unit == "MeV") {
        energy *= 1e6;
      } else if (unit == "keV") {
        energy *= 1e3;
      } else if (unit == "eV") {
        energy *= 1;
      } else {
        cout << "WARNING: Unknown unit for energy of primary" << endl;
      }
      SetEnergy(energy);
      foundEnergy = true;
    }

    found = line.find("Primary zenith angle");
    if (found!=std::string::npos) {
      stringstream ss(line);
      double zen;
      ss >> dum >> dum >> dum >> zen >> unit;
      if (unit == "deg") {
        zen *= M_PI/180;
      } else if (unit == "rad") {
        zen *= 1;
      } else {
        cout << "WARNING: Unknown unit for zenith angle" << endl;
      }
      SetZenithAngle(zen);
    }

    found = line.find("Primary azimuth angle");
    if (found!=std::string::npos) {
      stringstream ss(line);
      double azi;
      ss >> dum >> dum >> dum >> azi >> unit;
      if (unit == "deg" || unit == "deg.") {
        azi *= M_PI/180;
      } else if (unit == "rad") {
        azi *= 1;
      } else {
        cout << "WARNING: Unknown unit for azimuh angle" << endl;
      }
      SetAzimuthAngle(azi);
    }
    
    found = line.find("Geomagnetic field");
    if (found!=std::string::npos) {
      stringstream ss1(line);
      double Bint;
      ss1 >> dum >> dum >> dum >> Bint >> unit;
      if (unit == "nT") {
        Bint *= 1e-9;
      } else if (unit == "uT") {
        Bint *= 1e-6;
      } else if (unit == "mT") {
        Bint *= 1e-3;
      } else if (unit == "T") {
        Bint *= 1;
      } else if (unit == "uG") {
        Bint *= 1e-10;
      } else if (unit == "mG") {
        Bint *= 1e-7;
      } else if (unit == "Gs") {
        Bint *= 1e-4;
      } else if (unit == "gm") {
        Bint *= 1e-9;
      }
      SetGeomagneticIntesity(Bint);
      
      double Binc,Bdec,Bzen,Bazi;
      getline(ifs,line);
      stringstream ss2(line);
      ss2 >> dum >>  Binc >> unit >> dum >> Bdec >> unit;

      if (unit == "deg"|| unit == "deg.") {
        Bzen = 90. + Binc;
        Bazi = 0;//90. - Bdec;
        Bzen *= M_PI/180;
        Bazi *= M_PI/180;
      } else if (unit == "rad") {
        Bzen = (M_PI/2) + Binc;
        Bazi = 0;
      } else {
        cout << "WARNING: Unknown unit (" << unit << ") for magnetic field angles" << endl;
      
      }
//      Bazi += M_PI/2;
//      if (Bazi > 2 * M_PI) Bazi -= 2 * M_PI;
      SetGeomagneticAzimuth(Bazi);
      SetGeomagneticZenith(Bzen);
    }

    found = line.find("Sl. first interact. depth:");
    if (found!=std::string::npos) {
      stringstream ss(line);
      double Xfirst;
      ss >> dum >> dum >> dum >> dum >> Xfirst;
      SetXfirst(Xfirst);
    }
    
    found = line.find("Sl. depth of max. (g/cm2)");
    if (found!=std::string::npos) {
      stringstream ss(line);
      double Xmax;
      ss >> dum >> dum >> dum >> dum >> dum >> Xmax;
      SetXmax(Xmax);
    }


    found = line.find("Vt. first interact.");
    if (found!=std::string::npos) {
      stringstream ss(line);
      double XFirstVert;
      ss >> dum >> dum >> dum >> dum >> XFirstVert;
      SetXfirstVert(XFirstVert);
    }

    found = line.find("Vt. depth of max.");
    if (found!=std::string::npos) {
      stringstream ss(line);
      double XmaxVert;
      ss >> dum >> dum >> dum >> dum >> dum >> XmaxVert;
      SetXmaxVert(XmaxVert);
    }
    
    found = line.find("Ground altitude:");
    if (found!=std::string::npos) {
      stringstream ss(line);
      double GroundAlt;
      string unit;
      ss >> dum >> dum >> GroundAlt >> unit;
      if (unit == "km") {
        SetGroundAlt(GroundAlt * 1e3);
      } else if (unit == "m") {
        SetGroundAlt(GroundAlt);
      } else {
        cout << "WARNING: Unknown unit in setting ground altitude: " << unit << endl;
      
      }
    }


  }
}

void
ZHSEvent::PrintArray() {
  SayHi();
  if (fArray.size() == 0) {
    cout << "empty Array" << endl;
  } else {
    cout <<"Antenna Id \t X [m] \t\t Y [m] \t\t Z [m] \t timeStep [s] \t Start Time" << endl;
    for (int i = 0; i < fArray.size(); i++) {
      cout << fArray[i].fId << "\t\t " << fArray[i].fX<< "\t\t " << fArray[i].fY<< "\t\t " << fArray[i].fZ << "\t" << fArray[i].fTimeStep << "\t " << fArray[i].fStartTime <<  endl;
      
    }
  }

}

void
ZHSEvent::PrintEfield(int antId,string comp)//comp = x,y, or z
{
  int found = false;
  for (int i = 0; i < fArray.size(); i++) {
    if (antId == fArray[i].fId) {
      found = true;
          if (comp =="x") {
        for (int iv = 0; iv < fArray[i].fEx.size(); iv++)
          cout << setprecision(9) << fArray[i].fStartTime + (iv * fArray[i].fTimeStep) << "\t" << fArray[i].fEx[iv] << endl;
      } else if (comp == "y") {
        for (int iv = 0; iv < fArray[i].fEy.size(); iv++)
          cout << setprecision(9) <<  fArray[i].fStartTime + (iv * fArray[i].fTimeStep) << "\t" << fArray[i].fEy[iv] << endl;

      } else if (comp == "z") {
        for (int iv = 0; iv < fArray[i].fEz.size(); iv++)
          cout << setprecision(9) <<  fArray[i].fStartTime + (iv * fArray[i].fTimeStep) << "\t" << fArray[i].fEz[iv] << endl;
      } else {
        cout << "ERROR: unknown efield component in PrintEfield, it should be x,y or z" << endl;
        exit(0);
      }
    }
  }

  if (!found) {
    cout << "Error: no antenna found with Id: " << antId << endl;
    exit(0);
  }

}

void
ZHSEvent::PrintEfieldUser(int antId,string comp)//comp = x,y, or z
{
  int found = false;
  for (int i = 0; i < fArrayUser.size(); i++) {
    if (antId == fArrayUser[i].fId) {
      found = true;
      if (comp =="x") {
        for (int iv = 0; iv < fArrayUser[i].fEx.size(); iv++)
          cout << setprecision(9) << fArrayUser[i].fStartTime + (iv * fArrayUser[i].fTimeStep) << "\t" << fArrayUser[i].fEx[iv] << endl;
      } else if (comp == "y") {
        for (int iv = 0; iv < fArrayUser[i].fEy.size(); iv++)
          cout << setprecision(9) <<  fArrayUser[i].fStartTime + (iv * fArrayUser[i].fTimeStep) << "\t" << fArrayUser[i].fEy[iv] << endl;
        
      } else if (comp == "z") {
        for (int iv = 0; iv < fArrayUser[i].fEz.size(); iv++)
          cout << setprecision(9) << fArrayUser[i].fStartTime + (iv * fArrayUser[i].fTimeStep) << "\t" << fArrayUser[i].fEz[iv] << endl;
      } else {
        cout << "ERROR: unknown efield component in PrintEfieldUser, it should be x,y or z" << endl;
        exit(0);
      }
    }
  }
  
  if (!found) {
    cout << "Error: no antenna found with Id: " << antId << endl;
    exit(0);
  }
}

void
ZHSEvent::PrintEventInfo() {
  cout << "Event info " << endl;
  cout << "-----------" << endl;
  cout << "Zenith (deg):\t " <<  fZenith * 5.729578e+01 << endl;
  cout << "Azimuth (deg): \t"<<  fAzimuth * 5.729578e+01 << endl;
  cout << "Energy (EeV): \t " << fEnergy /1e18 << endl;
  cout << "Xmax (g/cm^-2): \t" << fXmax << endl;
  cout << "X first (g/cm^-2):" << fXfirst << endl;
  
}


Antenna
ZHSEvent::GetAntenna(int antId)
{
  for (int i = 0; i < fArray.size(); i++) {
    if (antId == fArray[i].fId)
      return fArray[i];
  }
  cout << "Error: no antenna found with Id: " << antId  << " in GetAntenna" << endl;
  exit(0);
}

Antenna
ZHSEvent::GetAntennaUser(int antId)
{
  for (int i = 0; i < fArrayUser.size(); i++) {
    if (antId == fArrayUser[i].fId)
      return fArrayUser[i];
  }
  cout << "Error: no antenna found with Id: " << antId  << " in GetAntennaUser" << endl;
  exit(0);
}


//Rectangular bandpass filter between lowFreq and highFreq. Frequencies in Hertz
void
ZHSEvent::Filter(double lowFreq, double highFreq)
{
  fArrayUser = fArray;
  for (int i = 0; i < fArray.size(); i++){
    double samFreq = 1/fArray[i].fTimeStep;
    fArrayUser[i].fEx.clear();
    fArrayUser[i].fEy.clear();
    fArrayUser[i].fEz.clear();
    RectangularFilter(fArray[i].fEx,fArrayUser[i].fEx,samFreq,lowFreq,highFreq);
    RectangularFilter(fArray[i].fEy,fArrayUser[i].fEy,samFreq,lowFreq,highFreq);
    RectangularFilter(fArray[i].fEz,fArrayUser[i].fEz,samFreq,lowFreq,highFreq);

    fArrayUser[i].fIsTimeDomain = true;
  }
  stringstream ss;
  ss << "Filter_Between_" << lowFreq <<"_"<<highFreq;
  fLastApplied = ss.str();

}



//calculate the envelope of the analytical signal using a hilbetr transform
void
ZHSEvent::Envelope()
{
  fArrayUser = fArray;
  for (int i = 0; i < fArray.size(); i++) {
    fArrayUser[i].fEx.clear();
    fArrayUser[i].fEy.clear();
    fArrayUser[i].fEz.clear();
    HilbertEnvelope(fArray[i].fEx,fArrayUser[i].fEx);
    HilbertEnvelope(fArray[i].fEy,fArrayUser[i].fEy);
    HilbertEnvelope(fArray[i].fEz,fArrayUser[i].fEz);
    fArrayUser[i].fIsTimeDomain = true;
  }

  stringstream ss;
  ss << "Envelope";
  fLastApplied = ss.str();
}

//Frequencies in Hertz
void
ZHSEvent::FilterAndEnvelope(double lowFreq, double highFreq)
{
  Filter(lowFreq,highFreq);
  vector<Antenna> fTemp = fArrayUser;
  for (int i = 0; i < fArray.size(); i++){
    double samFreq = 1/fArray[i].fTimeStep;
    fTemp[i].fEx.clear();
    fTemp[i].fEy.clear();
    fTemp[i].fEz.clear();
    HilbertEnvelope(fArrayUser[i].fEx,fTemp[i].fEx);
    HilbertEnvelope(fArrayUser[i].fEy,fTemp[i].fEy);
    HilbertEnvelope(fArrayUser[i].fEz,fTemp[i].fEz);
    fTemp[i].fIsTimeDomain = true;    
  }
  fArrayUser = fTemp;
  stringstream ss;
  ss << "Filter_Between_" << lowFreq <<"_"<<highFreq<<"_Envelope";
  fLastApplied = ss.str();
}


//Frequencies in Hertz
void
ZHSEvent::AddGaussNoise(double rms, bool randomSeed)
{
  TRandom3 r;
  if (randomSeed) r.SetSeed(0);
  for (int i = 0; i < fArray.size(); i++){
    for (int j = 0; j < fArray[i].fEx.size(); j++) {
      fArray[i].fEx[j] += r.Gaus(0,rms);
      fArray[i].fEy[j] += r.Gaus(0,rms);
      fArray[i].fEz[j] += r.Gaus(0,rms);
    }
  }
}

void
ZHSEvent::ScaleEfield(double k)
{
  TRandom3 r;
  for (int i = 0; i < fArray.size(); i++){
    for (int j = 0; j < fArray[i].fEx.size(); j++) {
      fArray[i].fEx[j] *= k;
      fArray[i].fEy[j] *= k;
      fArray[i].fEz[j] *= k;
    }
  }
}



//Calculates the amplitude spectrum
void
ZHSEvent::AmplitudeSpectrum()
{

  fArrayUser = fArray;
  for (int i = 0; i < fArray.size(); i++) {
    fArrayUser[i].fEx.clear();
    fArrayUser[i].fEy.clear();
    fArrayUser[i].fEz.clear();
    //normalization
    vector<float> vEx,vEy,vEz;
    for (int iv = 0; iv < fArray[i].fEx.size(); iv++) {
      vEx.push_back(fArray[i].fEx[iv] * fArray[i].fTimeStep);
      vEy.push_back(fArray[i].fEy[iv] * fArray[i].fTimeStep);
      vEz.push_back(fArray[i].fEz[iv] * fArray[i].fTimeStep);
    }
    
    AbsFFT(vEx,fArrayUser[i].fEx);
    AbsFFT(vEy,fArrayUser[i].fEy);
    AbsFFT(vEz,fArrayUser[i].fEz);
    fArrayUser[i].fIsTimeDomain = false;
  }
  stringstream ss;
  ss << "AmplitudeSpectrum";
  fLastApplied = ss.str();
}

void
ZHSEvent::PrintAntennaReco(Antenna& a) {
  cout << "Antenna ID:\t " << a.fId << endl;
  cout << "--------------" << endl;
  cout << " Peak Time:\t" << a.fPeakTime << endl;
  cout << " Stokes I:\t" << a.fI << endl;
  cout << " Stokes Q:\t" << a.fQ << endl;
  cout << " Stokes U:\t" << a.fU << endl;
  cout << " Stokes V:\t" << a.fV << endl;
  cout << " Fraction Polarized: " << a.fIp / a.fI << endl;
  cout << " Fraction Lin. Polarized: " << a.fL / a.fI << endl;
  cout << " Fraction Circ. Polarized: " << a.fV / a.fI << endl;
  cout << "Major axis A: " <<  a.fA << endl;
  cout << "Minor axis B: " <<  a.fB << endl;
  cout << "angle : " <<  a.fPhi << endl;
  

}

void
ZHSEvent::RecoAntenna(Antenna& a)
{
  cout << "RecoAntenna" << endl;
  vector<float> vI;
  ZHSUtil::TimeSeriesI(a.fEx,a.fEy,vI);
  vector<float> vU;
  ZHSUtil::TimeSeriesU(a.fEx,a.fEy,vU);
  vector<float> vV;
  ZHSUtil::TimeSeriesV(a.fEx,a.fEy,vV);
  vector<float> vQ;
  ZHSUtil::TimeSeriesQ(a.fEx,a.fEy,vQ);
  
  
  double maxI = 0;
  for (int i = 0; i < vI.size(); i++) {
    if (vI[i] > maxI) {
      maxI = vI[i];
      a.fPeakTime = i * a.fTimeStep;
    }
  }
  a.fWindowStart = a.fPeakTime -4.e-9;
  a.fWindow = 10.e-9;
  
  int  iStart = int(a.fWindowStart/a.fTimeStep);
  int  iStop = int((a.fWindowStart + a.fWindow)/a.fTimeStep);
  a.fI = 0;
  a.fQ = 0;
  a.fU = 0;
  a.fV = 0;
  cout <<"iStart:\t" <<iStart << "\t iStop: " << iStop << endl;
  for (int  i = iStart; i < iStop; i++) {
    a.fI += vI[i];
    a.fQ += vQ[i];
    a.fU += vU[i];
    a.fV += vV[i];
  }
  a.fI /= iStop - iStart;
  a.fQ /= iStop - iStart;
  a.fU /= iStop - iStart;
  a.fV /= iStop - iStart;
  
  
  a.fIp = sqrt(pow(a.fQ,2) + pow(a.fU,2) + pow(a.fV,2));
  a.fL = sqrt(pow(a.fQ,2) + pow(a.fU,2));
  a.fA = sqrt(0.5 *(a.fIp + a.fL));
  a.fB = sqrt(0.5 *(a.fIp - a.fL));
  a.fPhi = 0.5*atan2(a.fU,a.fQ);
  
}


