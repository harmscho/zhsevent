#include <TApplication.h>
#include <TGClient.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TRandom.h>
#include <TGButton.h>
#include <TRootEmbeddedCanvas.h>
#include <math.h>

#include <sstream>
#include "ZHSViewer.h"
#include "ZHSEvent.h"

using namespace std;


ZHSViewer::ZHSViewer(const TGWindow *p,UInt_t w,UInt_t h,string sryFile, string timeFile,int nPulse)
: TGMainFrame(p,w,h),
fSryFile(sryFile),
fTimeFile(timeFile)
 {
  // Creates widgets of the example
 
 
// // composite frame
// TGCompositeFrame *fMainFrame1091 = new TGCompositeFrame(fMainFrame1614,595,372,kVerticalFrame);
// fMainFrame1091->SetName("fMainFrame1091");
// fMainFrame1091->SetLayoutBroken(kTRUE);
 
 fTab = new TGTab(this,1200,500);
 AddFrame(fTab, new TGLayoutHints(kLHintsExpandX |kLHintsExpandY, 10,10,10,1));
 //Wave tab window
 fWaveTab = fTab->AddTab("Wav.");
 fWaveTab->SetLayoutManager(new TGVerticalLayout(fWaveTab));
 fWaveTab->SetLayoutBroken(kTRUE);
  fEcanvas = new TRootEmbeddedCanvas ("Ecanvas",fWaveTab,1000,400);
 fWaveTab->AddFrame(fEcanvas, new TGLayoutHints(kLHintsExpandX |kLHintsExpandY, 10,10,10,1));
 
 //Map window
 fMapTab = fTab->AddTab("Map.");
 fMapTab->SetLayoutManager(new TGVerticalLayout(fMapTab));
 fMapTab->SetLayoutBroken(kTRUE);

 fMapCanvas = new TRootEmbeddedCanvas ("Ecanvas",fMapTab,1000,500);
 fMapTab->AddFrame(fMapCanvas, new TGLayoutHints(kLHintsExpandX |kLHintsExpandY, 10,10,10,1));
// fMapCanvas->SetRightMargin(0.5);

 
 
  TGHorizontalFrame *hframe=new TGHorizontalFrame(this, 1200,40);
 //draw time
  TGTextButton *draw = new TGTextButton(hframe,"&Update");
  draw->Connect("Clicked()","ZHSViewer",this,"Update()");
  hframe->AddFrame(draw, new TGLayoutHints(kLHintsCenterX,5,5,3,4));

 //draw spec
// TGTextButton *drawSpec = new TGTextButton(hframe,"&DrawSpec");
// drawSpec->Connect("Clicked()","ZHSViewer",this,"DoDrawSpec()");
// hframe->AddFrame(drawSpec, new TGLayoutHints(kLHintsCenterX,5,5,3,4));
 
 //Exit the program
  TGTextButton *exit = new TGTextButton(hframe,"&Exit ","gApplication->Terminate()");
  hframe->AddFrame(exit, new TGLayoutHints(kLHintsCenterX,5,5,3,4));
 
 //draw mode
 drawModeSel = new TGComboBox(hframe,-1,kHorizontalFrame | kSunkenFrame | kDoubleBorder | kOwnBackground);
 drawModeSel->AddEntry("Time [x]",0);
 drawModeSel->AddEntry("Time [y]",1);
 drawModeSel->AddEntry("Time [z]",2);
 drawModeSel->AddEntry("Amp.Spec Mag",3);
 drawModeSel->AddEntry("Amp.Spec [x]",4);
 drawModeSel->AddEntry("Amp.Spec [y]",5);
 drawModeSel->AddEntry("Amp.Spec [z]",6);
 drawModeSel->AddEntry("Envelope [x]",7);
 drawModeSel->AddEntry("Envelope [y]",8);
 drawModeSel->AddEntry("Envelope [z]",9);
 drawModeSel->AddEntry("Bandpass [x]",10);
 drawModeSel->AddEntry("Bandpass [y]",11);
 drawModeSel->AddEntry("Bandpass [z]",12);
 drawModeSel->Resize(116,22);
 drawModeSel->Select(0);
 hframe->AddFrame(drawModeSel,new TGLayoutHints(kLHintsCenterX,5,5,3,4));
 
 //filter range
 fLowerFreq = new TGNumberEntry(hframe, (Double_t) 0,6,-1,(TGNumberFormat::EStyle) 5);
 hframe->AddFrame(fLowerFreq, new TGLayoutHints(kLHintsCenterX,5,5,3,4));
 
 fUpperFreq = new TGNumberEntry(hframe, (Double_t) 0,6,-1,(TGNumberFormat::EStyle) 5);
 hframe->AddFrame(fUpperFreq, new TGLayoutHints(kLHintsCenterX,5,5,3,4));
 
 //add frame to general frame
  AddFrame(hframe,new TGLayoutHints(kLHintsCenterX,2,2,2,2));
 
  // Sets window name and shows the main frame
  SetWindowName("ZHS Viewer");
  MapSubwindows();
  Resize(GetDefaultSize());
  MapWindow();
 cout << "READ EVENT..." << endl;
  fCurrentEvent = NULL;
  ReadEvent();
  Update();
}

void ZHSViewer::DoDraw() {
  // Draws function graphics in randomly chosen interval
/*  
  drawModeSel->AddEntry("Time [x]",0);
  drawModeSel->AddEntry("Time [y]",1);
  drawModeSel->AddEntry("Time [z]",2);
  drawModeSel->AddEntry("Amp.Spec Mag",3);
  drawModeSel->AddEntry("Amp.Spec [x]",4);
  drawModeSel->AddEntry("Amp.Spec [y]",5);
  drawModeSel->AddEntry("Amp.Spec [z]",6);
  drawModeSel->AddEntry("Envelope [x]",7);
  drawModeSel->AddEntry("Envelope [y]",8);
  drawModeSel->AddEntry("Envelope [z]",9);
  drawModeSel->AddEntry("Bandpass [x]",10);
  drawModeSel->AddEntry("Bandpass [y]",11);
  drawModeSel->AddEntry("Bandpass [z]",12);
  */
  TCanvas *fCanvas = fEcanvas->GetCanvas();
  fCanvas->cd();
  int drawMode = drawModeSel->GetSelected();
  std::cout << "Selected Mode:" << drawMode << std::endl;
  

  fCurrentEvent->PrintArray();
  int nAnt = fCurrentEvent->GetNAntennas();
  double color = 51;
  double dc = 50./nAnt;
  if (drawMode >= 3 && drawMode < 7) {
    fCurrentEvent->AmplitudeSpectrum();
  } else if (drawMode >= 7 && drawMode < 10) {
    fCurrentEvent->Envelope();
  } else if (drawMode >= 10 && drawMode < 13) {
    std::cout << "Bandpass Filter: " <<  fLowerFreq->GetNumber() * 1e6 << "\t" << fUpperFreq->GetNumber() * 1e6<< std::endl;
    fCurrentEvent->Filter(fLowerFreq->GetNumber() * 1e6,fUpperFreq->GetNumber() * 1e6);
  }
  string drawString;
  if (drawMode == 0 || drawMode ==4 || drawMode ==7 || drawMode ==10)
    drawString = "x";
  if (drawMode == 1 || drawMode ==5 || drawMode ==8 || drawMode ==11)
    drawString = "y";
  if (drawMode == 2 || drawMode ==6 || drawMode ==9 || drawMode ==12)
    drawString = "z";
  if (drawMode == 3)
    drawString = "mag";
  
  TGraph* gr;
  TGraph* gr1;
  double min,max,minAll,maxAll;
  minAll = +1e9;
  maxAll = -1e9;
  for (int iAnt = 1; iAnt <= nAnt; iAnt++) {
    Antenna a;
    if (drawMode < 3) {
      a = fCurrentEvent->GetAntenna(iAnt);
    } else {
      a = fCurrentEvent->GetAntennaUser(iAnt);
    }
    
    gr = AntennaToGraph(a,drawString);
    
    GetMinMaxInGraph(gr,min,max);
    if (min < minAll)
      minAll = min;
    if (max >maxAll)
      maxAll = max;
    
    gr->SetLineWidth(2);
    gr->SetLineColor(color);
    if (iAnt == 1) {
      gr1 = gr;
      gr->Draw("AL");
    } else  gr->Draw("SAMEL");
    color += dc;
  }

  gr1->SetMaximum(maxAll*1.1);
  gr1->SetMinimum(minAll*1.1);

  fCanvas->Update();

}

void ZHSViewer::Update(){
  cout << "DoDraw()" << endl;
  DoDraw();
  cout << "DoMap()" << endl;
  DrawMap();
  cout << "PrintInfo()" << endl;
  fCurrentEvent->PrintEventInfo();
}

void ZHSViewer::DrawMap() {
  int drawMode = drawModeSel->GetSelected();
  std::cout << "Selected Mode:" << drawMode << std::endl;
  

  int nAnt = fCurrentEvent->GetNAntennas();
  
  double X[nAnt];
  double Y[nAnt];
  int lineColor[nAnt];
  double peakX[nAnt];
  double peakY[nAnt];
  
  TGraph* grArray = new TGraph();
  TGraph* grWorld = new TGraph();
  double color = 51;
  double dc = 50./nAnt;
  if (drawMode >= 3 && drawMode < 7) {
    fCurrentEvent->AmplitudeSpectrum();
  } else if (drawMode >= 7 && drawMode < 10) {
    fCurrentEvent->Envelope();
  } else if (drawMode >= 10 && drawMode < 13) {
    std::cout << "Bandpass Filter: " <<  fLowerFreq->GetNumber() * 1e6 << "\t" << fUpperFreq->GetNumber() * 1e6<< std::endl;
    fCurrentEvent->Filter(fLowerFreq->GetNumber() * 1e6,fUpperFreq->GetNumber() * 1e6);
  }

  TGraph* grX;
  TGraph* grY;
//  TGraph* gr1;
  double min,max,minAll,maxAll;
  minAll = +1e9;
  maxAll = -1e9;
  double minX(+1e9),minY(+1e9),maxX(-1e9),maxY(-1e9);
  for (int iAnt = 1; iAnt <= nAnt; iAnt++) {
    Antenna a;
    if (drawMode < 3) {
      a = fCurrentEvent->GetAntenna(iAnt);
    } else {
      a = fCurrentEvent->GetAntennaUser(iAnt);
    }
    fCurrentEvent->RecoAntenna(a);
    fCurrentEvent->PrintAntennaReco(a);
    grX = AntennaToGraph(a,"x");
    grY = AntennaToGraph(a,"y");
    
    double maxPeak;
    double minPeak;
    
    peakX[iAnt-1] = GetExtremumInGraph(grX);
    peakY[iAnt-1] = GetExtremumInGraph(grY);
    
    if (peakX[iAnt-1] > maxAll)
      maxAll = peakX[iAnt-1];
    if (peakY[iAnt-1] > maxAll)
      maxAll = peakY[iAnt-1];

    lineColor[iAnt-1] = (int)color;
    color += dc;
    
    X[iAnt-1] = a.fX;
    Y[iAnt-1] = a.fY;
    grArray->SetPoint(iAnt-1,X[iAnt-1],Y[iAnt-1]);
    
    if (X[iAnt-1] >maxX)
      maxX = X[iAnt-1];
    if (Y[iAnt-1] > maxY)
      maxY = Y[iAnt-1];
    if (X[iAnt-1] < minX)
      minX = X[iAnt-1];
    if (Y[iAnt-1] <minY)
      minY = Y[iAnt-1];
    
  }

  if (fabs(maxX - minX) < 0.01) {
    minX = -1;
    maxX = 1;
  }
  if (fabs(maxY - minY) < 0.01) {
    minY = -1;
    maxY = 1;
  }
  double rangeX = maxX - minX;
  double rangeY = maxY - minY;
  
  grWorld->SetPoint(0,minX - 0.1*rangeX,minY - 0.1*rangeY);
  grWorld->SetPoint(1,maxX + 0.1*rangeX,maxY + 0.1*rangeY);
  
  TCanvas *fCanvas2 = fMapCanvas->GetCanvas();
//  gr1->SetMaximum(maxAll*1.1);
//  gr1->SetMinimum(minAll*1.1);
  fCanvas2->cd()->SetRightMargin(0.5);
  grWorld->SetNameTitle("grArray","Array Layout; X[m]; Y[m]");
  grArray->SetNameTitle("grArray","Array Layout; X[m]; Y[m]");
  grArray->SetMarkerStyle(28);
  grWorld->SetMarkerStyle(1);
  grWorld->SetMarkerColor(0);
  grWorld->Draw("AP");
  grArray->Draw("SAMEP");
  TLine* lineX;
  TLine* lineY;
  for (int i = 0; i < nAnt; i++) {
    lineX = new TLine();
    lineX->SetLineWidth(3);
    lineX->SetLineColor(lineColor[i]);
    lineY = new TLine();
    lineY->SetLineWidth(3);
    lineY->SetLineColor(lineColor[i]);
    double scaleX =  0.15 * rangeY * peakX[i]/maxAll;
    double scaleY =  0.15 * rangeY * peakY[i]/maxAll;

    lineX->DrawLine(X[i] - scaleX/2,Y[i] ,X[i] + scaleX/2,Y[i]);
    lineX->DrawLine(X[i] ,Y[i] - scaleY/2 ,X[i],Y[i] + scaleY/2);
  
  }
  
  fCanvas2->cd();

  fCanvas2->Update();
  
}

void
ZHSViewer::ReadEvent() {
  if (fCurrentEvent != NULL)
    delete fCurrentEvent;
  fCurrentEvent = new ZHSEvent(fTimeFile,fSryFile);
  
}

ZHSViewer::~ZHSViewer() {
  // Clean up used widgets: frames, buttons, layout hints
  fMain->Cleanup();
  if (fCurrentEvent != NULL)
    delete fCurrentEvent;
  delete fMain;
}


TGraph*
ZHSViewer::AntennaToGraph(Antenna a,string component,string name) {
  vector <float> v;
  if (component == "x") {
    v = a.fEx;
  } else if (component == "y") {
    v = a.fEy;
  }else if (component == "z") {
    v = a.fEz;
  } else if (component == "mag") {
    for (int i = 0; i < a.fEx.size(); i++)
      v.push_back(sqrt(pow(a.fEx[i],2) + pow(a.fEy[i],2) + pow(a.fEz[i],2)));
  } else if (component == "mag_hor") {
    for (int i = 0; i < a.fEx.size(); i++)
      v.push_back(sqrt(pow(a.fEx[i],2) + pow(a.fEy[i],2)));
  } else {
    cout << "ERROR: unknown component" << endl;
    exit(-1);
  }
  
  TGraph* gr = new TGraph();;
  stringstream ssName,ssTitle;
  double dx,xb;
  if (a.fIsTimeDomain) {
    xb = a.fStartTime;
    dx = fabs(a.fTimeStep);
    ssName << name <<"_time_" << component << "_" << a.fId;
    ssTitle << "time domain; time [ns]; electric field [V / m]";
  } else {
    xb = 0;
    dx = fabs(a.fFreqBin);
    ssName << name <<"_freq_" << component << "_" << a.fId;
    ssTitle << "frequency domain; frequency [MHz]; ASD [V m^{-1}MHz^{-1}]";
    
  }
  
  for (int i = 0; i < v.size(); i++) {
    if (a.fIsTimeDomain) {
      gr->SetPoint(i,(xb + i * dx)*1e9,v[i]);
    } else {
      gr->SetPoint(i,(xb + i * dx)/1e6,v[i]);//normalization to MHz
    }
  }
  
  gr->SetNameTitle(ssName.str().c_str(),ssTitle.str().c_str());
  return gr;
  
}


TGraph*
ZHSViewer::AntennaToGraph(Antenna a,string component) {
  return AntennaToGraph(a,component,"");
}


void
ZHSViewer::GetMinMaxInGraph(TGraph* gr,double& min, double& max)
{
  max = -1e10;
  min = +1e10;
  for (int i = 0; i < gr->GetN();i++) {
    double val = gr->GetY()[i];
    if (val > max)
      max = val;
    if (val< min)
        min = val;
  }
}

double
ZHSViewer::GetExtremumInGraph(TGraph* gr)
{
  double extr = 0;
  for (int i = 0; i < gr->GetN();i++) {
    double val = fabs(gr->GetY()[i]);
    if (val > extr)
      extr = val;
  }
  return extr;
}


