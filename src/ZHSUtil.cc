#include "ZHSUtil.h"

void ZHSUtil::SayHi(){
  cout<<"Hi"<<endl;
}

void ZHSUtil::FFT(vector<float>& v_in, fftw_complex* &fftout){
  fftw_complex *fftin=NULL;
  int len=v_in.size();

  fftin = (fftw_complex *)fftw_malloc(sizeof(fftw_complex)*len);
  fftout = (fftw_complex *)fftw_malloc(sizeof(fftw_complex)*len);
  fftw_plan fftpf;
  fftpf=fftw_plan_dft_1d(len,fftin,fftout,FFTW_FORWARD,FFTW_MEASURE);
	for(int i=0;i<len;i++){
    fftin[i][0] = v_in.at(i);
		fftin[i][1] = 0.;
  }
  fftw_execute(fftpf);
  for(int i=0;i<len;i++){
    fftout[i][0] = fftout[i][0];///sqrt(len);
    fftout[i][1] = fftout[i][1];///sqrt(len);
  }
  fftw_free(fftin);
  fftw_destroy_plan(fftpf);

}

void ZHSUtil::InvFFT(fftw_complex* &fftin,vector<float>& v_out, int len){
  v_out.clear();
	fftw_complex *fftout=NULL;
  fftw_complex *fft_in=NULL;
  fftout = (fftw_complex *)fftw_malloc(sizeof(fftw_complex)*len);
  fft_in = (fftw_complex *)fftw_malloc(sizeof(fftw_complex)*len);
  fftw_plan fftpb;
  fftpb=fftw_plan_dft_1d(len,fft_in,fftout,FFTW_BACKWARD,FFTW_MEASURE);
  
	for (int j = 0; j < len; j++) {//dont understand why i should copy it!!??
    fft_in[j][0] = fftin[j][0];
    fft_in[j][1] = fftin[j][1];
  }
  
	fftw_execute(fftpb);
  for (int j = 0; j < len; j++) {
		v_out.push_back(fftout[j][0]/len);//sqrt(len));//real part assuming that the imag part = 0
  }	
 
	 fftw_destroy_plan(fftpb);
//  fftw_free(fft_in);
 // fftw_free(fftout);
  //fftw_free(fftin);
  delete[] fft_in;
  delete[] fftin;
  delete[] fftout;
}

void ZHSUtil::FFTInvFFT (vector<float>& v_in, vector<float>& v_out)
{
  fftw_complex* ftrace = NULL;
  FFT(v_in,ftrace); 
  InvFFT(ftrace,v_out,v_in.size());
}

void ZHSUtil::AbsFFT(vector<float>& v_in,vector<float>& v_out){
  v_out.clear();
  fftw_complex* ftrace;
  FFT(v_in,ftrace);
  for(int  j = 0; j< v_in.size()/2; j++){
    v_out.push_back(sqrt(2) * sqrt(ftrace[j][0]*ftrace[j][0]+ftrace[j][1]*ftrace[j][1]));
  }
  //fftw_free(ftrace);
  delete[] ftrace;
}

void ZHSUtil::PhaseFFT(vector<float>& v_in,vector<float>& v_out)
{
  v_out.clear();
  fftw_complex* ftrace;
  FFT(v_in,ftrace);
  for(int  j = 0; j< v_in.size(); j++) {
	  v_out.push_back(atan2(ftrace[j][1],ftrace[j][0]));
  }
  //fftw_free(ftrace);
  delete[] ftrace;
}

void ZHSUtil::HilbertEnvelope(vector<float>& v_in,vector<float>& v_out){
  fftw_complex *fftin=NULL,*fftout=NULL;
  int len = v_in.size();
  v_out.clear();

  fftin = (fftw_complex *)fftw_malloc(sizeof(fftw_complex)*len);
  fftout = (fftw_complex *)fftw_malloc(sizeof(fftw_complex)*len);
  fftw_plan fftpf,fftpb;
  fftpf=fftw_plan_dft_1d(len,fftin,fftout,FFTW_FORWARD,FFTW_MEASURE);
  fftpb=fftw_plan_dft_1d(len,fftout,fftin,FFTW_BACKWARD,FFTW_MEASURE);
  int i;
  float tmp;
  for (i = 0; i < len ;i++) {
    fftin[i][0] = v_in.at(i);
    fftin[i][1] = 0.;
  }
  fftw_execute(fftpf);
  for(i = 0; i < len; i++){
    if(i < (len / 2)) {
      tmp = fftout[i][1];
      fftout[i][1] = -fftout[i][0];
    } else {
      tmp = -fftout[i][1];
      fftout[i][1] = fftout[i][0];
    }
    fftout[i][0] = tmp;
  }
  fftw_execute(fftpb);
  for(i=0;i<len;i++){
    v_out.push_back(sqrt((fftin[i][0]*fftin[i][0]+fftin[i][1]*fftin[i][1])/(len*len)+v_in.at(i)*v_in.at(i)));
  }   
//  fftw_free(fftin);
  //fftw_free(fftout);
  delete[] fftout;
  delete[] fftin;
  fftw_destroy_plan(fftpb);  
  fftw_destroy_plan(fftpf);  
}

void ZHSUtil::MedianFilter(vector<float>& v_in, vector<float>& v_out,int WindowSize = 3)
{
  v_out.clear();
  if (WindowSize % 2 != 1) {
    std::cout << "WindowSize must be a odd number " <<std::endl;
  }
  int Edge = WindowSize / 2;
  vector<float> FilterBuffer;
  for (int i = 0; i < v_in.size(); i++) {
    FilterBuffer.clear();
		for (int j = i - Edge; j <=i + Edge; j++) {
      if (j < 0)
	    FilterBuffer.push_back(v_in[0]);
			else if (j > v_in.size())
				FilterBuffer.push_back(v_in[v_in.size() - 1]);
			else
	    FilterBuffer.push_back(v_in[j]);
		}
    sort(FilterBuffer.begin(),FilterBuffer.end());
    v_out.push_back(FilterBuffer[Edge]);
  }  
}

void ZHSUtil::AbsFFTMedianFilter(vector<float>& v_in, vector<float>& v_out) 
{
  int WindowSize = 3;
  AbsFFTMedianFilter(v_in, v_out, WindowSize);
}
void ZHSUtil::AbsFFTMedianFilter(vector<float>& v_in, vector<float>& v_out,int WindowSize = 3) 
{
 vector<float> v_out_temp;
 AbsFFT(v_in,v_out_temp);
 MedianFilter(v_out_temp,v_out,WindowSize);
}

void ZHSUtil::RFISupressionMedianFilter(vector<float>& v_in, vector<float>& v_out,int WindowSize)
{
  vector<float> v_abs;
  vector<float> v_phase;
  AbsFFTMedianFilter(v_in,v_abs,WindowSize);
  PhaseFFT(v_in,v_phase);

  fftw_complex* fftin;
  fftin = (fftw_complex *)fftw_malloc(sizeof(fftw_complex)*v_in.size());
//  cout << "still fine" <<endl;
  for (int j = 0; j < v_in.size(); j++) {
//    cout <<j <<"\t"<<v_in.size()<<endl;
	fftin[j][0] = v_abs[j]*cos(v_phase[j]);
    fftin[j][1] = v_abs[j]*sin(v_phase[j]); 
  }
  InvFFT(fftin,v_out,v_in.size());
}

void ZHSUtil::RectangularFilter(vector<float>& v_in,vector<float>& v_out,double sample_freq,double low_freq,double high_freq){

  int TraceLength = v_in.size();
  int LowEdge = int(low_freq * v_in.size() / sample_freq);
  int HighEdge = int(high_freq * v_in.size() / sample_freq);
	//  cout << "LowEdge " << LowEdge << "\tHighEdge " << HighEdge << " sample_freq " << sample_freq << " int(TraceLength/2) +1: " << int(TraceLength/2) +1 << endl;
  fftw_complex *filt = NULL;
  FFT(v_in,filt);

  for (int j = 0; j < int(TraceLength/2); j++) {
    if (j < LowEdge || j > HighEdge) {
			filt[j][0] = 0;
			filt[j][1] = 0;
			filt[TraceLength - j - 1][0] = 0;
			filt[TraceLength - j - 1][1] = 0;
		}
  }
  InvFFT(filt,v_out,TraceLength);

}

void ZHSUtil::TimeShiftFrequencyDomain(vector<float>& v_in,vector<float>& v_out,double sample_freq, double dt){
  
  int TraceLength = v_in.size();

  fftw_complex *fshift = NULL;
  FFT(v_in,fshift);
  double fRange = sample_freq/2;
  double df = fRange / (TraceLength/2);
  double f;
  for (int j = 0; j < int(TraceLength/2); j++) {
    f = double(j) * df;
    double phi = atan2(fshift[j][1],fshift[j][0]);
    double dphi = f * dt * 2 * M_PI;
    phi -= dphi;
    double fAbs = sqrt(pow(fshift[j][0],2) + pow(fshift[j][1],2));
    fshift[j][0] = fAbs * cos(phi);
    fshift[j][1] = fAbs * sin(phi);
    

    fshift[TraceLength - j ][0] = fAbs * cos(-phi);
    fshift[TraceLength - j ][1] = fAbs * sin(-phi);
  }
  InvFFT(fshift,v_out,TraceLength);
  
}




void ZHSUtil::HannFunction(vector<float>& v_in, vector<float>& v_out, double TraceFraction = 0.1)
{
 double alpha = 0.5;
 int length = v_in.size();
 double rise = length * TraceFraction;
 double fall = length * (1-TraceFraction);
 v_out.clear();
 int WindowSize = 2 * int(rise);
 double Weightfkt;
 for (int i = 0; i < length; i++) {
   if (i < rise) {
     Weightfkt = alpha * (1 - cos(2 * M_PI *i / (WindowSize-1)));
   }else if(i > fall)
     Weightfkt = alpha * (1 - cos(2 * M_PI * (int(i - fall + rise)) / (WindowSize-1))); 
   else 
     Weightfkt = 1;
   v_out.push_back(v_in[i] * Weightfkt);
 }
}


void ZHSUtil::GroupDelay(vector<float>& v_in,vector<float>& v_out,double df) {
  fftw_complex *fft = NULL;
  FFT(v_in,fft);
  v_out.clear();
  double tg = 0;
  for (int i = 0; i < v_in.size()-1; i++){
    double phi1 = atan2(fft[i][1],fft[i][0]);
    double phi2 = atan2(fft[i+1][1],fft[i+1][0]);
    
    tg = -acos(cos(phi2 - phi1))/ (2 * df * M_PI);
    v_out.push_back(tg);
  }
}

///Stokes Parameters
void ZHSUtil::HilbertTransform(vector<float>& v_in,vector<float>& v_out){
  fftw_complex *fftin=NULL,*fftout=NULL;
  int len = v_in.size();
  v_out.clear();
  
  fftin = (fftw_complex *)fftw_malloc(sizeof(fftw_complex)*len);
  fftout = (fftw_complex *)fftw_malloc(sizeof(fftw_complex)*len);
  fftw_plan fftpf,fftpb;
  fftpf=fftw_plan_dft_1d(len,fftin,fftout,FFTW_FORWARD,FFTW_MEASURE);
  fftpb=fftw_plan_dft_1d(len,fftout,fftin,FFTW_BACKWARD,FFTW_MEASURE);
  int i;
  float tmp;
  for (i = 0; i < len ;i++) {
    fftin[i][0] = v_in.at(i);
    fftin[i][1] = 0.;
  }
  fftw_execute(fftpf);
  for(i = 0; i < len; i++){
    if(i < (len / 2)) {
      tmp = fftout[i][1];
      fftout[i][1] = -fftout[i][0];
    } else {
      tmp = -fftout[i][1];
      fftout[i][1] = fftout[i][0];
    }
    fftout[i][0] = tmp;
  }
  fftw_execute(fftpb);
  for(i=0;i<len;i++){
    v_out.push_back(fftin[i][0]/len);
  }
  //  fftw_free(fftin);
  //fftw_free(fftout);
  delete[] fftout;
  delete[] fftin;
  fftw_destroy_plan(fftpb);
  fftw_destroy_plan(fftpf);
}
void ZHSUtil::TimeSeriesI(vector<float>& v_in_x,vector<float>& v_in_y,vector<float>& v_out){
  vector<float> v_im_x;
  HilbertTransform(v_in_x,v_im_x);
  vector<float> v_im_y;
  HilbertTransform(v_in_y,v_im_y);
  v_out.clear();
  for (int i = 0; i < v_in_x.size(); i++) {
    v_out.push_back(v_in_x[i] * v_in_x[i] + v_im_x[i]*v_im_x[i] + v_in_y[i] * v_in_y[i] + v_im_y[i]*v_im_y[i] );
  }
}

void ZHSUtil::TimeSeriesQ(vector<float>& v_in_x,vector<float>& v_in_y,vector<float>& v_out){
  vector<float> v_im_x;
  HilbertTransform(v_in_x,v_im_x);
  vector<float> v_im_y;
  HilbertTransform(v_in_y,v_im_y);
  v_out.clear();
  for (int i = 0; i < v_in_x.size(); i++) {
    v_out.push_back(v_in_x[i] * v_in_x[i] + v_im_x[i]*v_im_x[i] - v_in_y[i] * v_in_y[i] - v_im_y[i]*v_im_y[i] );
  }
}
void ZHSUtil::TimeSeriesU(vector<float>& v_in_x,vector<float>& v_in_y,vector<float>& v_out){
  vector<float> v_im_x;
  HilbertTransform(v_in_x,v_im_x);
  vector<float> v_im_y;
  HilbertTransform(v_in_y,v_im_y);
  v_out.clear();
  for (int i = 0; i < v_in_x.size(); i++) {

    v_out.push_back(2 * (v_in_x[i] * v_in_y[i] + v_im_x[i]*v_im_y[i]));
  }
}
void ZHSUtil::TimeSeriesV(vector<float>& v_in_x,vector<float>& v_in_y,vector<float>& v_out){
  vector<float> v_im_x;
  HilbertTransform(v_in_x,v_im_x);
  vector<float> v_im_y;
  HilbertTransform(v_in_y,v_im_y);
  v_out.clear();
  for (int i = 0; i < v_in_x.size(); i++) {
    v_out.push_back(2 * (v_im_x[i] * v_in_y[i] - v_im_y[i]*v_in_x[i]));
  }
}









