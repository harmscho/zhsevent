#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include "ZHSEvent.h"
//#include "Atmosphere.h"
#include "TFile.h"
#include "TGraph.h"
#include "TProfile2D.h"
#include "TMath.h"
using namespace std;

TGraph AntennaToGraph(Antenna a,string component,string name) {
  vector <float> v;
  if (component == "x") {
    v = a.fEx;
  } else if (component == "y") {
    v = a.fEy;
  }else if (component == "z") {
    v = a.fEz;
  } else if (component == "mag"){
    for (int i = 0; i < a.fEx.size(); i++)
      v.push_back(sqrt(pow(a.fEx[i],2) + pow(a.fEy[i],2) + pow(a.fEz[i],2)));
  } else {
    cout << "ERROR: unknown component" << endl;
    exit(-1);
  }
  
  TGraph gr;
  stringstream ssName,ssTitle;
  double dx,xb;
  if (a.fIsTimeDomain) {
    xb = a.fStartTime;
    dx = a.fTimeStep;
    ssName << "Antenna"<< a.fId <<"_time_" << component;
    ssTitle << "time domain; time [s]; electric field [V / m]";
  } else {
    xb = 0;
    dx = a.fFreqBin;
    ssName << "Antenna"<< a.fId <<"_freq_" << component;
    ssTitle << "frequency domain; frequency [Hz]; ASD [Vm^{-1}Hz^{-1}]";
    
  }
  
  for (int i = 0; i < v.size(); i++) {
    if (a.fIsTimeDomain) {
      gr.SetPoint(i,xb + i * dx,v[i]);
    } else {
      gr.SetPoint(i,xb + i * dx,v[i] * v[i]);//normalization to MHz
    }
  }
  
  gr.SetNameTitle(ssName.str().c_str(),ssTitle.str().c_str());
  return gr;
  
}


TGraph AntennaToGraph(Antenna a,string component) {
  return AntennaToGraph(a,component,"");
}

double MaxInGraph(TGraph gr){
  double maximum = -1e9;
  for (int i = 0; i < gr.GetN();i++) {
    double val = gr.GetY()[i];
    if (val > maximum) {
      maximum = val;
    }
  }
  return maximum;
  
}

double GetOffAxisAngle(double zen, double azi, Antenna& a) {
  cout << zen  << "\t" << azi << "\t" << a.fX << "\t" << a.fY << "\t" << a.fZ <<  endl;
  
  double ax = cos(azi) * sin(zen);
  double ay = sin(azi) * sin(zen);
  double az = cos(zen);
  
  double len = sqrt((a.fX * a.fX) + (a.fY * a.fY) + (a.fZ * a.fZ));
  cout << "len: " << len << endl;
  
  double offaxis = acos((ax * a.fX + ay * a.fY + az * a.fZ)/len);
  cout << offaxis * TMath::RadToDeg() << endl;
  return  offaxis * TMath::RadToDeg();
}


int main(int argc,char** argv) {
  
  if (argc < 4) {
    cout << "Usage: zhsToRoot <sry-file> <timefresnel-root.dat file> <output.root> <optional low freq MHz> <optional high freq MHz> " << endl;
    cout << "or zhsToRoot <sry-file> <timefresnel-root.dat file> <output.root>" << endl;
    return 0;
  }
  
  
  TFile f(argv[3],"recreate");
  
  ZHSEvent ev(argv[2],argv[1]);
  double low,high;
  double dpsi = 1;
  bool filter = false;
  if (argc >= 6) {
    low = atof(argv[4]);
    high = atof(argv[5]);
    filter = true;
    if (argc == 7) {
      dpsi = atof(argv[6]);
    } else dpsi = 1;
    
  }
  
//  if (argc == 5) {
//    dpsi = atof(argv[4]);
//  } else if (argc < 5) dpsi = 1;
  
  
  if (filter){
    ev.Filter(low*1e6,high*1e6);
  } else {
    ev.AmplitudeSpectrum();
  }
  
  TGraph grLDF;
  if (dpsi == 1) {
    grLDF.SetNameTitle("grLDF","LDF; antenna id; Peak |#vec{E}(t)| [V/m]");
  } else grLDF.SetNameTitle("grLDF","LDF; #psi [#circ]; Peak |#vec{E}(t)| [V/m]");
  
  TGraph gr;
  for (int i = 1; i <= ev.GetNAntennas(); i++) {
    //time domain
    
    cout << "processing antenna " << i << endl;
    if (!filter) {
      Antenna a = ev.GetAntenna(i);
      double psi = GetOffAxisAngle(ev.GetZenithAngle(),ev.GetAzimuthAngle(),a);
      gr = AntennaToGraph(a,"x","");
      gr.Write();
      gr = AntennaToGraph(a,"y","");
      gr.Write();
      gr = AntennaToGraph(a,"z","");
      gr.Write();
      gr = AntennaToGraph(a,"mag","");
      
      
      grLDF.SetPoint(grLDF.GetN(),psi,MaxInGraph(gr));
      //frequency domain
      a = ev.GetAntennaUser(i);
      gr = AntennaToGraph(a,"x","");
      gr.Write();
      a = ev.GetAntennaUser(i);
      gr = AntennaToGraph(a,"y","");
      gr.Write();
      a = ev.GetAntennaUser(i);
      gr = AntennaToGraph(a,"z","");
      gr.Write();
    } else {
      Antenna a = ev.GetAntennaUser(i);
      double psi = GetOffAxisAngle(ev.GetZenithAngle(),ev.GetAzimuthAngle(),a);
     	gr = AntennaToGraph(a,"x","");
      gr.Write();
      a = ev.GetAntennaUser(i);
      gr = AntennaToGraph(a,"y","");
      gr.Write();
      a = ev.GetAntennaUser(i);
      gr = AntennaToGraph(a,"z","");
      gr.Write();
      gr = AntennaToGraph(a,"mag","");
      if (dpsi == 1) {
        grLDF.SetPoint(grLDF.GetN(),i,MaxInGraph(gr));
      } else grLDF.SetPoint(grLDF.GetN(),(i-1)*dpsi,MaxInGraph(gr));
      
      
    }
  }
  grLDF.Write();
  f.Close();
  
  return 0;
}
