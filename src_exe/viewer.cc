#include <TApplication.h>
#include <TGClient.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TRandom.h>
#include <TGButton.h>
#include <TRootEmbeddedCanvas.h>

#include "ZHSViewer.h"
using namespace std;
void example(string timeFile, string sryFile, int nPulse = 0) {
  // Popup the GUI...
  cout << "Opening ZHSViewer with: " << endl;
  cout <<  "\tTime file: " << timeFile << endl;
  cout << "\t Sry file: " << sryFile << endl;
  if (nPulse == 0 ) {
    new ZHSViewer(gClient->GetRoot(),800,800,sryFile,timeFile);
  }else
    new ZHSViewer(gClient->GetRoot(),800,800,sryFile,timeFile,nPulse);
}

int main(int argc, char **argv) {
  cout << "===== ZHS Viewer  ===== " << endl;
  if (argc > 3 || argc < 2) {
    cout << "Usage: ./viewer <time-file> <sry-file>" << endl;
    return 0;
  }
  
  
  string timeFile = argv[1];
  string sryFile = argv[2];
  int nPulse = 0;
  if (argc == 4) {
    nPulse = atoi(argv[3]);
  } else nPulse = 0;
  cout <<"The Application" << endl;
  TApplication theApp("App", &argc, argv);

  cout << "Opening the window... " << endl;
  example(timeFile,sryFile,nPulse);
  theApp.Run();
  return 0;
}
